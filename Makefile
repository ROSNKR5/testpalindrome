PROJECT_ROOT = $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
CXX:=g++
OBJS = tesPalindrome.o
BUILD_DIR = ./build

ifeq ($(BUILD_MODE),debug)
	CFLAGS += -g
else ifeq ($(BUILD_MODE),run)
	CFLAGS += -O2
endif

all:	dir	tesPalindrome

tesPalindrome:	$(OBJS)
	$(CXX) -o $@ $^

%.o:	$(PROJECT_ROOT)%.cpp
	$(CXX) -c $(CFLAGS) $(CXXFLAGS) $(CPPFLAGS) -o $@ $<

%.o:	$(PROJECT_ROOT)%.c
	$(CC) -c $(CFLAGS) $(CPPFLAGS) -o $@ $<

dir: TEST_DIR

TEST_DIR:
	@set -e;\
	if [ ! -d ${BUILD_DIR} ]; then\
		mkdir -p ${BUILD_DIR}/default; \
	fi
clean:
	rm -fr tesPalindrome $(OBJS) $(BUILD_DIR)
