#include "tesPalindrome.h"
/*

bool isPal (char* st) {
    if(!st){
        return false;
    }
    int len = strlen(st);
    int flag = 0, il =0, ir = 0;
    char *lptr, *rptr;
    lptr = st;
    rptr = (st + (len - 1));
    while((lptr + il) <= (rptr -ir)){
        if(!(*( lptr + il) == *(rptr - ir))){
            if(flag){
                return false;

            }
            else{
                if((*( lptr + il+1) == *(rptr - ir))){
                    il +=1;
                }
                else if((*( lptr +il ) == *(rptr - ir -1))){
                    ir +=1;
                }
                else{
                    return false;
                }
                flag = 1;
            }

        }
        il++;
        ir++;
    }
    return true;
}
*/


bool isPal (char* st) {
    if(!st){
        return false;
    }
    int len = strlen(st);
    int flag = 0, il =0, ir = 0;
    char *lptr, *rptr;
    lptr = st;
    rptr = (st + (len - 1));
    while((lptr) <= (rptr)){
        if(!(*( lptr) == *(rptr))){
            if(flag){
                return false;

            }
            else{
                if((*( lptr +1) == *(rptr ))){
                    lptr++;
                }
                else if((*( lptr) == *(rptr -1))){
                    rptr--;
                }
                else{
                    return false;
                }
                flag = 1;
            }

        }
        lptr++;
        rptr--;
    }
    return true;
}
void usage(void)
{
	cout << "\n\n Please Provide a palindrome string\n\n";
}

int main(int argc, char *argv[])
{
	char *string_palindrome;
	if (argc > 1) {
		string_palindrome = argv[1];
	}
	else{
		usage();
		return 0;
	}
	if( isPal (string_palindrome) ){
		cout << "\n\ngiven string \"" << string_palindrome << "\" is a palindrome(At most one charcter allowed)\n\n";
	}
	else {
		cout << "\n\ngiven string \"" << string_palindrome << "\" is not a palindrome(At most one charcter allowed)\n\n";

	}

	return 0;
}
